function dialog_ac(divDialogo)
{
	var divDlgMain=divDialogo;
	var iAltoDef=200;
	var iAnchoDef=250;
	var iAltoDlg=0;
	var iAnchoDlg=0;
	
	this.modalCerrar=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align=\"center\" colSpan=\"4\">'+
				'<input  type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />&nbsp;&nbsp;&nbsp;'+
				'&nbsp;&nbsp;&nbsp;<input  type=\"button\" id=\"btnCancelar\" value=\"Cancelar\" class=\"BtnModal\" />'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnCancelar').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { closeNavegador(); };
	};
	
	this.modalcierre=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align="center" colSpan="4">'+
				'<input type=\"button\" id=\"btnModalAcep\" value=\"Aceptar\" class=\"BtnModal\" />'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { closeNavegador(); };
		document.getElementById('btnModalAcep').onclick=function() { closeNavegador(); };
	};

	this.modalcierremensaje=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
					'<td><button id=\"btnCerrarDlgx\" class=\"botonDialogo btndlg btndlg-cerrar\">X</button></td>'+					
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align="center" colSpan="4">'+
				'<input type=\"button\" id=\"btnModalAcep\" value=\"Si\" class=\"BtnModal\" />'+
				'&nbsp;&nbsp;&nbsp;<input type=\"button\" id=\"btnCancelar\" value=\"No\" class=\"BtnModal\" />'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
		document.getElementById('btnCerrarDlgx').onclick=function() { cerrar(); };
		document.getElementById('btnCancelar').onclick=function() { cerrar(); };
		document.getElementById('btnModalAcep').onclick=function() { closeNavegador(); };
	};
	
	
	this.modalpopup=function(iAncho, iAlto, sTitulo)
	{
		divDlgMain.innerHTML="";
		if(iAlto=='undefined' || iAlto==0) 
			iAltoDlg=iAltoDef;
		else
			iAltoDlg=iAlto;
		if(iAncho=='undefined' || iAncho==0) 
			iAnchoDlg=iAnchoDef;
		else
			iAnchoDlg=iAncho;

		if(sTitulo=='undefined') sTitulo='';
		var sHtml='<div id=\"divDialog\"><table border=\"0\" width=\"'+iAnchoDlg+'\" height=\"'+iAltoDlg+'\"">'+
				'<tr>'+
					'<td class=\"tituloDialogo\">'+ sTitulo +'</td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"top\" height=\"2%\"><hr/></td></tr>'+
				'<tr>'+
					'<td colspan=\"2\" valign=\"top\"><div id=\"divContentDlg\" class=\"divContentDlg\" ></div></td>'+
				'</tr>'+
				'<tr><td colspan=\"2\" valign=\"bottom\" height=\"2%\"><hr/></td></tr>'+
				'<tr><td colspan=\"2\" height=\"5%\"></td></tr>'+
				'<tr><td align="center" colSpan="4">'+
				'</td></tr>'+
			'</table></div>';
		divDlgMain.innerHTML=sHtml;
	};

	//-----------------------------------------------------------------------------------
	
	this.mostrar=function(sContenido)
	{
		document.getElementById('divDialog').style.width=iAnchoDlg+'px';
		document.getElementById('divDialog').style.height=iAltoDlg+'px';
		divContentDlg = document.getElementById('divContentDlg');
		divContentDlg.innerHTML=sContenido;
		//document.getElementById('divDialog').style.marginTop = ((document.body.clientHeight/2) - (iAltoDlg/2)) +  'px';
		//document.getElementById('divDialog').style.marginTop = ((500) - (400)) +  'px';
    	divDlgMain.style.display = 'block';
	};
	
	function cerrar(){
		divDlgMain.innerHTML="";
		divDlgMain.style.display = 'none';
	};
	
	this.agregarMensaje=function(sContenido)
	{
		divContentDlg = document.getElementById('divContentDlg');
		divContentDlg.innerHTML=divContentDlg.innerHTML+sContenido;
	};

	this.mostrarDialogo=function()
	{
		document.getElementById('divDialog').style.width=iAnchoDlg+'px';
		document.getElementById('divDialog').style.height=iAltoDlg+'px';
		document.getElementById('divDialog').style.marginTop = ((document.body.clientHeight/2) - (iAltoDlg/2)) +  'px';
    	divDlgMain.style.display = 'block';
	};
}

function closeNavegador()
{
	var OSName="Desconocido";
	
	if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
	if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
	if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
	if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";
	if (navigator.appVersion.indexOf("Android")!=-1) OSName="Android";


	if(OSName == "Android"){
		Android.volverMenuIne('0');
		//Android.terminarSolConst("1");
	}else{
		//firefox
		if(navigator.appName.indexOf('Netscape') >= 0 )
		{
			//NAVEGADOR FIREFOX
			javascript:window.close();
		}
		else 
		{
			if(navigator.appName.indexOf('Microsoft') >= 0)
			{//internet explorer
				var ventana = window.self;
				ventana.opener = window.self;
				ventana.close();
			}
		}
	}	
}
