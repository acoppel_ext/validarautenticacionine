/**************************************************************************************************************************************
 * <VARIABLES ESTATICAS DE CONFIGURACION>
**************************************************************************************************************************************/
var cTituloModal = 'AUTENTICACIÓN INE';
var ligaCase = 'php/caseine.php';
var RUTA_HUELLAS_INE = '/sysx/progs/web/entrada/huellas/ine/';
var RUTA_LOCAL_HUELLAS = 'C:\\TEMP\\huellas';
var ligaCaseLlamada = '';

/**************************************************************************************************************************************
 * <CATALOGO DE LIGAS "cat_enlacesine" para la ejecuón de EXE´s>
**************************************************************************************************************************************/
var iOpcion = 0;
var VALIDAR_HUELLAS = 2;
var SUBIR_ARCHIVO = 3;
var COMPARAR_MINUCIAS = 4;
var iOpcionHuella = 0;
var PUBLICA_HUELLAS_IZQ = 1;
var PUBLICA_HUELLAS_DER = 2;
var URL_LLAMADA_IVR = 9;
var URL_CONSENTIMIENTO = 11;
var iMilisegundos = 0;
var esUtileria = 0;

/*
	El campo iUtileria hace referencia de donde se está ejecutando la pagina:

	iUtileria = 0 Pagina detonada desde Constancias afiliacion para traspasos por modulo
	iUtileria = 1 Pagina detonada traspaso por movil
	iUtileria = 2 Pagina detonada desde servicios
	iUtileria = 3 Pagina detonada desde Constancias afiliacion para terceras figuras
*/

/**************************************************************************************************************************************
 * <VARIABLES GLOBALES DE CONFIGURACION>
**************************************************************************************************************************************/
var curpyfechader = '';
var curpyfechaizq = '';
var cCurp = '';
var iEmpleado = 0;
var iRenapo = 0;
var huellas = 0;
var iTipoAfiliacion = 0;
var ipmodulo = '';
var nombres = '';
var appaterno = '';
var apmaterno = '';
var numemisionine = 0;
var clvelectorine = 0;
var anioregine = 0;
var cicine = 0;
var ocrine = 0;
var telefono = 0;
var companiacel = '';
var compania = '';
var folioapi = '';
var medioenvio = 0;
var contadorintentos = 0;
var mensajeTiempo = '';
var porcentajemin = 0;
var numerointentos = 0;
var iContHuellas = 0;
var ipoflline = '';
var reenviointerno = 0;
var intentoreenvio = 0;
/* variables globales para JSON de INE */

var bCurp = false;
var bOcr = false;
var bCic = false;
var bNombres = false;
var bAppaterno = false;
var bApmaterno = false;
var bAnioregistro = false;
var bAnioemision = false;
var bNumeroemision = false;
var bClaveelector = false;
var cConsultawsine = 'NO';
var cConsultawsineServ = 'NO';
var cConsultaservine = '';
var cSimilitudindiceizqwsq = 0;
var cSimilitudindicederwsq = 0;
var tFechatimestamp = '1900-01-01T01:01:01.001';
var cFolioine = '';
var accionmsj = '';
var mensajeCR = '';
var latitud = '';
var longitud = '';
var tiendamod = 0;
var banderaActIvr = 0;
var dEnvio = '';
var fechaServidor = '';
var resultado ='';
var contador = 0;
var contadori = 0;
var mensajeT='';
/**************************************************************************************************************************************
 * <INCIO DE ACCIONES EN LA PAGINA>
**************************************************************************************************************************************/
$(document).ready(function () {
	mdlEspere(cTituloModal);
	sistemaOperativo();
	$("#capturahuellas").prop('disabled', true);

	/*Obtener Variables pasadas por URL*/
	if (OSName == 'Android') {
		iEmpleado = getParameterByName('empleado');
		cCurp = getParameterByName('curp');
		iRenapo = getParameterByName('renapo');
		iUtileria = 1;
	}else{
		iEmpleado = obtenerVarialbesURL('empleado');
		cCurp = obtenerVarialbesURL('curp');
		iRenapo = obtenerVarialbesURL('renapo');
		huellas = obtenerVarialbesURL('huellas');
		iUtileria = parseInt(obtenerVarialbesURL('utileria'));
		iTipoAfiliacion = obtenerVarialbesURL('tipoafiliacion');

		$("#valine").prop('disabled', true);
		validarCampos();
	}

	if(OSName == "Android"){

		$('#apellidom, #apellidop, #nombres').on('textInput', e => {

			var keyCode = e.originalEvent.data.charCodeAt(0);

			if(!(keyCode >= 65 && keyCode<= 90 || keyCode >= 97 && keyCode <= 122 || keyCode == 32  || keyCode == 39 || keyCode >= 45 && keyCode <= 47 || keyCode == 95 || keyCode >= 152 && keyCode <= 154 || keyCode == 129 || keyCode == 132 || keyCode == 137 || keyCode == 139 || keyCode == 142 || keyCode == 148))
			{
				e.preventDefault();
			}
	   });

	   var numemision_type = document.getElementById("numemision");
	   numemision_type.type = "tel";

	   var anioreg_type = document.getElementById("anioreg");
	   anioreg_type.type = "tel";

	   var cicine_type = document.getElementById("cicine");
	   cicine_type.type = "tel";

	   var ocrine_type = document.getElementById("ocrine");
	   ocrine_type.type = "tel";

	   /*var celular_type = document.getElementById("celular");
	   celular_type.type = "tel";*/

	   var celular_type = document.getElementById("Confcelular");
	   celular_type.type = "tel";

	   $('#numemision, #anioreg, #cicine, #ocrine, #celular, #Confcelular').on('textInput', e => {

			var keyCode = e.originalEvent.data.charCodeAt(0);

			if(!(keyCode >= 48 && keyCode<= 57 ))
			{
				e.preventDefault();
			}
		});

	}

	/*Validamos el empleado*/
	if ((iEmpleado != "") && (validarEmpleado(iEmpleado))) {

		if (validarnumerointentos()) {
			obteneripserv();
			validarcompanias();
			validarenvio();
			validarrenapo("'" + cCurp + "'");
			// formato de curps variables globales : Generar curp formato
            fechaServidor = consultarFechaServidor();
			//Obtener los nombres de los archivos donde se guardan el md5 de las huellas capturadas
			curpyfechaizq = cCurp + "_" + fechaServidor + "I.txt";			
			curpyfechader = cCurp + "_" + fechaServidor + "D.txt";

			if(huellas == 1){
				console.log("recaptura");
				recapturadatosine();
			}
			else{
				console.log("validacion");
				validacionHuellasTrabajador();
			}

		} else {
			if (OSName == 'Android') {
				var sMensaje = mensajeTiempo;
				var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
				myDlg.modalcierre(500, 250, cTituloModal);
				myDlg.mostrar(sMensaje);
			} else {
				mdlMsjCerrar(cTituloModal, mensajeTiempo);
			}
		}
	}

	$("#btncancelar").click(function () {
		var mensajeCerrar2 = "";

		if (OSName == 'Android') {
			var sMensaje = "Promotor, ¿ deseas cancelar la validacion INE?";
			var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
			myDlg.modalCerrar(500, 250, cTituloModal);
			myDlg.mostrar(sMensaje);
		} else {
			if (iUtileria == 2) { //Cuando entra por servicio el mensaje va con colaborador
				var mensajeCerrar2 = 'Colaborador, ¿Deseas cancelar la validación INE?';
				var btn1 = 'Si';
				var btn2 = 'No';
				mdlMsjCerrarPreg(cTituloModal, mensajeCerrar2, btn1, btn2);
			}
			else {
				//var mensajeCerrar2 = 'Promotor, ¿Deseas cancelar la validación INE?';
				if (iTipoAfiliacion != 26 && iTipoAfiliacion != 33) {
					mensajeCerrar2 = 'Promotor, ¿ deseas cancelar la validacion INE?';
					var btn1 = 'Si';
					var btn2 = 'No';
					mdlMsjCerrarPreg(cTituloModal, mensajeCerrar2, btn1, btn2);
				}
				else {
					mensajeCerrar2 = 'Promotor, al no realizar Autenticación INE no podrás continuar con la afiliación, ¿deseas cancelar la Autenticación INE?';
					var btn1 = 'Si';
					var btn2 = 'No';
					mdlMsjCerrarPreg(cTituloModal, mensajeCerrar2, btn1, btn2);
				}
			}
		}
	});
	radiobtn = document.getElementById("ird1");
	radiobtn.checked = true;

	$('input[type=radio][name=rdval]').change(function () {
		if (this.value == 1) {
			$("#cic").show();
			$(".bloquearbotones").prop('disabled', false);
			$(".dtIFE").hide();
			$(".dtINE").show();
		}
		else if (this.value == 2) {
			$("#cic").hide();
			$("#cicine").val('');
			$(".bloquearbotones").prop('disabled', false);
			$(".dtINE").hide();
			$(".dtIFE").show();
		}
		mostrarImgIneIfe(this.value);
	});

	$('#Confcelular').keyup(function () {
		$('#alertcorreo').remove('');
		var ConfCel = $('#Confcelular').val();
		if (ConfCel.length == 10) {
			var celCapt = $('#celular').val();
			if (celCapt == ConfCel) {
				$('#Confcelular').focusout();
			}
			else {
				$('#Confcelular').attr('type', 'text');
				$('#Confcelular').val('');
				$(".input-group #" + "Confcelular").parent().after("<div id = 'alertcorreo' class='alert alert-danger'>El teléfono celular capturado no coincide</div>");
			}
		}
	});
	
	$('#medioenvio').change(function () {
		hablitarValidarIne();
	});
	
	$.validator.setDefaults({
		submitHandler: function (form) {

			if($("#apellidop").val() == "" && $("#apellidom").val() == "")
			{
				mdlMsjFunc("AUTENTICACION INE", "Promotor: Favor de capturar Apellido Paterno o Materno del Trabajador", 'Aceptar', "");
				return;
			}

			mdlEspere(cTituloModal);
			$("#valine").prop('disabled', true);
			nombres = $("#nombres").val();
			appaterno = $("#apellidop").val() == "" ? "XX" : $("#apellidop").val();
			apmaterno = $("#apellidom").val() == "" ? "XX" : $("#apellidom").val();
			numemisionine = $("#numemision").val();
			clvelectorine = $("#clvelector").val();
			medioenvio = $("#medioenvio").val();
			anioregine = $("#anioreg").val();
			cicine = $("#cicine").val();
			ocrine = $("#ocrine").val();
			telefono = $("#celular").val();
			telefonoConf = $("#Confcelular").val();
			companiacel = $("#compania").val();

			$('#Confcelular').attr('type', 'password');
			$("#celular").prop('disabled', true);
			$("#Confcelular").prop('disabled', true);

			if(appaterno == "XX" && apmaterno == "XX")
			{
				mdlMsjFunc("AUTENTICACION INE", "Promotor: Favor de capturar Apellido Paterno o Materno del Trabajador", 'Aceptar', "");
				$("#valine").prop('disabled', false);
				return;
			}

			detonarServicioIne(1);
		}
	});

	$("#formVal").validate({
		rules: {
			nombre: { required: true, minlength: 1, maxlength: 50 },
			apellidop: { minlength: 1, maxlength: 50 },
			apellidom: { minlength: 1, maxlength: 50 },
			tipocreden: { required: true },
			numemision: { required: true, minlength: 2, maxlength: 2 },
			clvelector: { required: true, minlength: 18, maxlength: 18 },
			anioreg: { required: true, minlength: 4, maxlength: 4 },
			cicine: { required: true, minlength: 9, maxlength: 10 },
			ocrine: { required: true, minlength: 13, maxlength: 13 },
			celular: { required: true, minlength: 10, maxlength: 10 },
			Confcelular: { required: true, minlength: 10, maxlength: 10 },
			compania: { required: true, minlength: 1 },
			medioenvio: { required: true, minlength: 1 }
		},
		errorElement: "small",
		errorPlacement: function (error, element) {
			error.addClass("help-block-jqval col-md-12");
			element.parents(".input-val").addClass("has-feedback-jqval");
			if (element.prop("type") === "checkbox" || element.prop("type") === "radio") { error.insertAfter(element.parent("label")); }
			else { error.insertAfter(element); }
			// Add the span element, if doesn't exists, and apply the icon classes to it.
			if (!element.next("span")[0]) { $("<span class='fa fa-times feedback-jqval'></span>").insertAfter(element); }
		},
		success: function (element) {
			// Add the span element, if doesn't exists, and apply the icon classes to it.
			if (!element.next("span")) { $("<span class='fa fa-check feedback-jqval'></span>").insertAfter(element); }
		},
		highlight: function (element) {
			$(element).parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
			$(element).next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
		},
		unhighlight: function (element) {
			$(element).parents(".input-val").removeClass("has-error-jqval").addClass("has-success-jqval");
			$(element).next("span").removeClass("fa-times").addClass("fa-check");
		}
	});
	fnactivaivr();
	$('*').bind("cut copy paste", function (e) { e.preventDefault(); });
});

/**************************************************************************************************************************************
 * <FUNCIONES GENERALES>
**************************************************************************************************************************************/
function datosPagina() {
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 2 },
		success: function (data) {
			if (data.irespuesta == 4) {
				$("#txtFechaDia").val(data.fechaActual);
				ipServidorSPA = data.ipspa;
				ipmodulo = data.ipmodulo;
			}
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function validarEmpleado(empleado) {
	var bResp = false;
	var arrResp = new Array();
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS01", arrdatos: empleado },
		beforeSend: function () {
			mdlEspere(cTituloModal);
		}, success: function (data) {
			arrResp = (data.CONS01);
			if (data.irespuesta == 4) {
				if (arrResp[0].icodigo == 0) {
					bResp = true;
					cCurpPromotor = arrResp[0].curppromotor;
					$("#txtUsuario").val(arrResp[0].cnombre);
					$("#txtNomTienda").val(arrResp[0].ctienda);
					datosPagina();
				} else {
					if (OSName == 'Android') {
						var sMensaje = arrResp[0].cmensaje;
						var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
						myDlg.modalcierre(500, 250, cTituloModal);
						myDlg.mostrar(sMensaje);
					} else {
						mdlMsjCerrar(arrResp[0].cmensaje);
					}
				}
			} else {
			}
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});

	return bResp;
}

function validarcompanias() {
	var arrResp = new Array();
	var sHtml = '';
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS04" },
		success: function (data) {
			arrResp = (data.CONS04);
			sHtml += ('<option value ="">SELECCIONE...</option>');
			for (var i = 0; i < arrResp.length; i++) {
				sHtml += ("<option value =" + arrResp[i].clavec + ">" + arrResp[i].nombre.toUpperCase() + "</option>");
			}
			$('#compania').empty();
			$('#compania').html(sHtml);

		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function validarrenapo(curp) {
	var arrResp = new Array();
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS06", arrdatos: curp },
		success: function (data) {
			arrResp = (data.CONS06);
			if (iRenapo == 1) {
				/*$("#nombres").val(utf8_decode(arrResp[0][0] != '' ? arrResp[0][0] : 'XX'));
				$("#apellidop").val(utf8_decode(arrResp[0][1] != '' ? arrResp[0][1] : 'XX'));
				$("#apellidom").val(utf8_decode(arrResp[0][2] != '' ? arrResp[0][2] : 'XX'));*/
				$("#nombres").val(utf8_decode(arrResp[0][0]));
				$("#apellidop").val(utf8_decode(arrResp[0][1]));
				$("#apellidom").val(utf8_decode(arrResp[0][2]));
			}
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function validarenvio() {
	var arrResp = new Array();
	var sHtml = '';
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS05" },
		success: function (data) {
			arrResp = (data.CONS05);
			sHtml += ('<option value ="">SELECCIONE UNA OPCIÓN</option>');
			for (var i = 0; i < arrResp.length; i++) {
				sHtml += ("<option value =" + arrResp[i].iestatus + ">" + arrResp[i].cdescripcion.toUpperCase() + "</option>");
				dEnvio += arrResp[i].iestatus;
			}
			$('#medioenvio').empty();
			$('#medioenvio').html(sHtml);

		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function validarnumerointentos() {
	var bResp = false;
	var arrResp = new Array();
	var datosCons = COMPARAR_MINUCIAS + " , '" + cCurp + "'";
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS07", arrdatos: datosCons },
		success: function (data) {
			arrResp = data;

			if (arrResp.CONS07[0].repuesta == 0)
				bResp = true;

			numerointentos = arrResp.CONS07[0].numerointentos;
			if (iUtileria == 2) { //remplazar el mensaje para cuando la pagina entre por servicios
				arrResp.CONS07[0].mensaje = arrResp.CONS07[0].mensaje.replace('Promotor', 'Colaborador');
				arrResp.CONS07[0].mensaje = arrResp.CONS07[0].mensaje.replace(', te recordamos que tambi&eacuten puedes realizar la Afiliaci&oacuten de Traspaso con la Constancia de Folio de Estado de Cuenta BanCoppel o con el Estado de Cuenta de la Afore actual del Trabajador', '.');
			}

			mensajeTiempo = arrResp.CONS07[0].mensaje;
			porcentajemin = parseFloat(arrResp.CONS07[0].porcentaje);
			iMilisegundos = arrResp.CONS07[0].milisegundos;
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});

	return bResp;
}

function consultarFechaServidor()
{
    var retorno;
    $.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONSFECH" },
		success: function (data) {            
            var fecha = data.CONSFECH[0][0];
            retorno = fecha;
        }
        , error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
    });
    
    return retorno;
}

function obteneripserv() {
	var arrResp = new Array();
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS11" },
		success: function (data) {
			arrResp = data;
			ipoflline = arrResp.CONS11[0].ipofflinelnx;
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function obtenermensajecodigoresp(opcionmsj) {
	var arrResp = new Array();
	var idcons = iTipoAfiliacion == "26" || iTipoAfiliacion == "33" ? "CONS18" : "CONS12";
	var datosCons = "";
	var iIVR = 0;
	if(huellas == 1){iIVR = 0}else{iIVR = esUtileria}

	if (idcons == "CONS12")
		datosCons = opcionmsj + "," + iIVR;
	else
		datosCons = opcionmsj;

	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: idcons, arrdatos: datosCons },
		success: function (data) {
			arrResp = data;
			if (idcons == "CONS12")
				mdlMsjFunc(cTituloModal, arrResp.CONS12[0].mensaje, 'Aceptar', arrResp.CONS12[0].accion);
			else
				mdlMsjFunc(cTituloModal, arrResp.CONS18[0].mensaje, 'Aceptar', arrResp.CONS18[0].accion);
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function obtenertiendamodulo(ipmod) {
	var bResp = 0;
	var arrResp = new Array();

	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS13", arrdatos: ipmod },
		success: function (data) {
			arrResp = data;
			tiendamod = arrResp.CONS13[0].itienda;
			bResp = 0;
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});

	return bResp;
}

function recapturadatosine() {
	mdlEspere(cTituloModal);
	iContHuellas = 0;
	parametrosHuellas = curpyfechaizq + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_INE + " " + ipoflline + " 0";
	publicahuellasizq(parametrosHuellas);
	$("#valine").prop('disabled', false);
}

function recapturahuellas() {
	mdlEspere(cTituloModal);
	validacionHuellasTrabajador();
	validarbotonesine();
	$("#valine").prop('disabled', false);
}

/**************************************************************************************************************************************
 * <EJECUCION DE EXES SERVICIO WEBX>
**************************************************************************************************************************************/
function validacionHuellasTrabajador() {
	iOpcion = VALIDAR_HUELLAS;
	var parametros = cCurp + " " + RUTA_LOCAL_HUELLAS + " "+ fechaServidor;	
	opcionEjecuta(parametros);
}

function publicahuellasder(parametros) {
	iContHuellas++;
	iOpcion = SUBIR_ARCHIVO;
	iOpcionHuella = PUBLICA_HUELLAS_DER;
	opcionEjecuta(parametros);
}

function publicahuellasizq(parametros) {
	iContHuellas++;
	iOpcion = SUBIR_ARCHIVO;
	iOpcionHuella = PUBLICA_HUELLAS_IZQ;
	opcionEjecuta(parametros);
}

/**************************************************************************************************************************************
 * <DETONAR SERVICIO WEBX>
**************************************************************************************************************************************/
function obtenerligas(idliga) {
	var bResp = '';
	var arrResp = new Array();
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS09", arrdatos: idliga },
		beforeSend: function () {
		}, complete: function () {
		}, success: function (data) {
			arrResp = data;
			bResp = arrResp.CONS09[0].liga;

		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});

	return bResp;
}

function opcionEjecuta(sParametros) {
	var sRuta = obtenerligas(iOpcion);
	if (OSName == 'Android') {
		iEmpleado = getParameterByName('empleado');
		cCurp = getParameterByName('curp');
		sTipoSolicitud = 27;
		Android.llamaComponenteHuellasIne(5, iEmpleado, 1, 2, iEmpleado, sTipoSolicitud, cCurp);
	} else {
		ejecutaWebService(sRuta, sParametros);
	}
}

function ejecutaWebService(sRuta, sParametros) {
	if (OSName != 'Android') {
		console.log("sRuta-> " + sRuta + " sParametros-> " + sParametros);
		soapData = "",
			httpObject = null,
			docXml = null,
			iEstado = 0,
			sMensaje = "";
		sUrlSoap = "http://127.0.0.1:20044/";

		soapData =
			'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
			'<SOAP-ENV:Envelope' +
			' xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"' +
			' xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"' +
			' xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"' +
			' xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"' +
			' xmlns:ns2=\"urn:ServiciosWebx\">' +
			'<SOAP-ENV:Body  SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">' +
			'<ns2:ejecutarAplicacion>' +
			'<inParam>' +
			'<Esperar>1</Esperar>' +
			'<RutaAplicacion>' + sRuta + '</RutaAplicacion>' +
			'<parametros><![CDATA[' + sParametros + ']]></parametros>' +
			'</inParam>' +
			'</ns2:ejecutarAplicacion>' +
			'</SOAP-ENV:Body>' +
			'</SOAP-ENV:Envelope>';

		httpObject = getHTTPObject();

		if (httpObject) {
			if (httpObject.overrideMimeType) {
				httpObject.overrideMimeType("false");
			}
			httpObject.open('POST', sUrlSoap, false); //-- no asincrono
			httpObject.setRequestHeader("Accept-Language", null);
			httpObject.onreadystatechange = function () {
				if (httpObject.readyState == 4 && httpObject.status == 200) {
					parser = new DOMParser();
					docXml = parser.parseFromString(httpObject.responseText, "text/xml");
					iEstado = docXml.getElementsByTagName('Estado')[0].childNodes[0].nodeValue;
					//Llamada a la función que recibe la respuesta del WebService
					respuestaWebService(iEstado);
				}
				else {
					console.log(httpObject.status);
				}
			};
			httpObject.send(soapData);
		}
	}

}

function getHTTPObject() {
	var xhr = false;
	if (window.ActiveXObject) {
		try { xhr = new ActiveXObject("Msxml2.XMLHTTP"); }
		catch (e) {
			try { xhr = new ActiveXObject("Microsoft.XMLHTTP"); }
			catch (e) { xhr = false; }
		}
	} else if (window.XMLHttpRequest) {
		try { xhr = new XMLHttpRequest(); }
		catch (e) { xhr = false; }
	}
	return xhr;
}

function respuestaWebService(iRespuesta) {
	console.log('iopcion ->' + iOpcion + ' irespuesta ->' + iRespuesta);
	switch (iOpcion) {
		case VALIDAR_HUELLAS:
			//iRespuesta = 1;
			if (iRespuesta == 1) {
				iContHuellas = 0;
				parametrosHuellas = curpyfechaizq + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_INE + " " + ipoflline + " 0";
				publicahuellasizq(parametrosHuellas);
			} else if (iRespuesta == 99) {
				if (OSName == 'Android') {
					Android.volverMenuIne('0');
				} else {
					cerrarNavegador();
				}
			} else {
				if (iUtileria == 2) {
					var mensajefnc = 'Colaborador, se presentó un problema al capturar las huellas de los dedos índices del Solicitante, favor de capturar nuevamente.';
					mdlMsjFunc(cTituloModal, mensajefnc, 'Aceptar', 'recapturahuellas();');
				}
				else {
					var mensajefnc = 'Promotor, se presentó un problema al capturar las huellas de los dedos índices del Trabajador, favor de capturar nuevamente.';
					mdlMsjFunc(cTituloModal, mensajefnc, 'Aceptar', 'recapturahuellas();');
				}
			}
			break;

		case SUBIR_ARCHIVO:
			switch (iOpcionHuella) {
				case PUBLICA_HUELLAS_IZQ:
					//iRespuesta = 1;
					if (iRespuesta == 1) {
						iContHuellas = 0;
						parametrosHuellas = curpyfechader + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_INE + " " + ipoflline + " 0";
						publicahuellasder(parametrosHuellas);
					}
					else {
						if (iContHuellas < 3) {
							parametrosHuellas = curpyfechaizq + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_INE + " " + ipoflline + " 0";
							publicahuellasizq(parametrosHuellas);
						}
						else {
							if (iUtileria == 2) {
								mdlMsjFunc(cTituloModal, 'Colaborador, se presentó un problema al capturar las huellas de los dedos índices del Solicitante, favor de capturar nuevamente.', 'aceptar', 'recapturahuellas();');
							}
							else {
								mdlMsjFunc(cTituloModal, 'Promotor, se presentó un problema al capturar las huellas de los dedos índices del Trabajador, favor de capturar nuevamente.', 'aceptar', 'recapturahuellas();');
							}
						}
					}
					break;
				case PUBLICA_HUELLAS_DER:
					//iRespuesta = 1;
					if (iRespuesta == 1) {
						iContHuellas = 0;
						obtenertiendamodulo("'" + ipmodulo + "'");
						$("#divContIne").show();
						mdlDesbloquearMsj();
						$(".dtINE").show();
					}
					else {
						if (iContHuellas < 3) {
							parametrosHuellas = curpyfechader + " " + RUTA_LOCAL_HUELLAS + " " + RUTA_HUELLAS_INE + " " + ipoflline + " 0";
							publicahuellasder(parametrosHuellas);
						}
						else {
							if (iUtileria == 2) {
								mdlMsjFunc(cTituloModal, 'Colaborador, se presentó un problema al capturar las huellas de los dedos índices del Solicitante, favor de capturar nuevamente.', 'aceptar', 'recapturahuellas();');
							}
							else {
								mdlMsjFunc(cTituloModal, 'Promotor, se presentó un problema al capturar las huellas de los dedos índices del Trabajador, favor de capturar nuevamente.', 'aceptar', 'recapturahuellas();');
							}
						}
					}
					break;

				default:
					break;
			}
			break;

		default:
			break;
	}
}

/**************************************************************************************************************************************
 * <CONSUMIR API>
**************************************************************************************************************************************/
function guardarDatosIne() {
	var bResp = false;

	appaterno = appaterno.replace(/'/gi, "''");
	apmaterno = apmaterno.replace(/'/gi, "''");
	nombres = nombres.replace(/'/gi, "''");

	if (OSName == 'Android') {
		esUtileria = 1;
	}
	else {
		esUtileria = iUtileria;
	}

	var arrResp = new Array();
	var arrDatos = {
		"reenviointerno": reenviointerno,
		"ocr": ocrine,
		"cic": cicine,
		"apellidoPaterno": appaterno,
		"apellidoMaterno": apmaterno,
		"nombre": nombres,
		"anioRegistro": anioregine,
		"numeroEmisionCredencial": numemisionine,
		"claveElector": clvelectorine,
		"curp": cCurp,
		"minucia2": curpyfechader,
		"minucia7": curpyfechaizq,
		"iEmpleado": iEmpleado,
		"ipModulo": ipmodulo,
		"esUtileria": esUtileria //llena el campo "utileria" de la tabla "ineautenticacionhuellas" donde el 0 indica que fue por la constancia
	};

	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 4, idcons: "guardarDatosIne", arrdatos: arrDatos },
		success: function (data) {
			arrResp = (data);
			if (arrResp.estatus == 5) {

				if (arrResp.respuesta.estatus == 1) {

					bResp = true;
				} else {
					if (iUtileria == 2) {
						mdlMsj(cTituloModal, "Colaborador, se presentó un problema al validar la ine favor de volver a intentar.");
						$("#valine").prop('disabled', false);
					}
					else {
						mdlMsj(cTituloModal, "Promotor, se presentó un problema al validar la ine favor de volver a intentar.");
						$("#valine").prop('disabled', false);
					}
				}
			} else {
				mdlMsj(cTituloModal, "consumo api fallo");
			}
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});

	return bResp;
}

function ejecutarApiINE() {
	var bResp = false;
	var arrResp = new Array();
	var arrDatos = {
		"curp": cCurp,
		"numEmpleado": iEmpleado,
		"latitud": latitud,
		"longitud": longitud
	};

	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 4, idcons: "auntenticacionIne", arrdatos: arrDatos },
		success: function (data) {
			arrResp = data;
			if (arrResp.estatus == 5) {
				bResp = true;
				if (arrResp.respuesta.registros.respondioServicio == 1) {
					bResp = true;
					folioapi = arrResp.respuesta.registros.foliosolicitud;
					//alert("folioapi: " + folioapi);
			/////////////////////////////comentar o Borrar folioapi=0 //////////////
				grabarLog('Funcion ejecutarApiINE(): folioapi --> ' + folioapi);

				//folioapi = 0;
					if(folioapi == 0 || folioapi == '' || folioapi == null)
					{
						if(contador < 2)
						{
							contador ++;
							ejecutarApiINE();
						}else
						{
							bResp = false;
							resultado = bResp;
							contador = 0;
						}						
					}else
					{
						bResp = true;
						resultado = bResp;
					}					

				} else {
					mdlMsj(cTituloModal, "consumo api fallo auntenticacionIne 1");
				}
			} else {
				mdlMsj(cTituloModal, "consumo api fallo auntenticacionIne 2");
			}
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});

	return bResp;
}

function obtenerUbicacion() {
	var bResp = false;
	var arrResp = new Array();
	var arrDatos = {
		"idTienda": 5,
	};

	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 4, idcons: "obtenerUbicacion", arrdatos: arrDatos },
		success: function (data) {
			arrResp = (data);
			latitud = arrResp.respuesta.registros[0].latitud;
			longitud = arrResp.respuesta.registros[0].longitud;
			if (arrResp.estatus == 5) {
				if (arrResp.respuesta.registros[0].respuesta == 1) {
					bResp = true;
				} else {
					mdlMsj(cTituloModal, "Respuesta api fallo obtenerUbicacion");
				}
			} else {
				mdlMsj(cTituloModal, "consumo api fallo obtenerUbicacion 2");
			}
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});

	return bResp;
}

function actualizaBitacoraIne() {
	var bResp = false;
	var arrResp = new Array();
	var arrDatos = {
		"foliosolicitud": folioapi,
		"curp": bCurp,
		"ocr": bOcr,
		"cic": bCic,
		"nombres": bNombres,
		"apPaterno": bAppaterno,
		"apMaterno": bApmaterno,
		"anioRegistro": bAnioregistro,
		"anioEmision": bAnioemision,
		"numeroEmisionCredencial": bNumeroemision,
		"claveElector": bClaveelector,
		"consutawsIne": cConsultawsine,
		"consultaServicioIne": cConsultawsineServ,
		"similitudIndiceIzqwsq": cSimilitudindicederwsq,
		"similitudIndiceDerwsq": cSimilitudindicederwsq,
		"fechaIneSalida": tFechatimestamp,
		"folioIne": cFolioine,
	};
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 4, idcons: "actualizaBitacoraIne", arrdatos: arrDatos },
		success: function (data) {
			arrResp = (data);
			var respuestaactualiza = arrResp.respuesta.registros[0].respuesta;
			if (arrResp.estatus == 5) {
				if (respuestaactualiza == 1) {
					bResp = true;
				}
			}
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});

	return bResp;
}

function consultardatosine(opcEnvio) {
	var arrResp = new Array();
	var arrDatos = { "foliosolicitud": folioapi };
	var datosJSON = new Array();
	var bResp = false;
	var bRespOpc = 0;
	var cRespServicio = 0; //Catalogo SELECT codigo FROM catrespuestaine;
	var iRespDatos = 0;
	var iRespMinucias = 0;
	var cRespSitucacion = '';
	var cRespRobo = '';
	var bRespMinucias = false;
	var datosinsertarsms = '';
	//alert("folioapi->" + folioapi);//BORRAR
	contadori ++ ;
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 4, idcons: "obtenerRespuestaIne", arrdatos: arrDatos },
		success: function (data) {
			arrResp = (data);
			if(opcEnvio == 1){
				contadorintentos++;
			}
			//alert("dentro de la funcion consultardatosine " + resultado);
			
			if(resultado)
			{
				//alert("Entro con folio -> " + folioapi);
					grabarLog("Entro con folioapi -> " + folioapi);
					console.log("foliosolicitud->" + folioapi);
				if (arrResp.respuesta.registros[0].respuesta == 1) {
					var txtjson = arrResp.respuesta.registros[0].json;
					datosJSON = JSON.parse(txtjson);

					console.log("codigo-> " + arrResp.respuesta.registros[0].codigo);
					console.log("JSONResp");
					console.log(datosJSON);

					//Obtener codigo respuesta de WS-INE
					if (arrResp.respuesta.registros[0].codigo != null) {
						cConsultaservine = arrResp.respuesta.registros[0].codigo;
						bResp = true;
					}

					/*
					* Validar respuesta del WS-INE
					* Validamos que el response tenga datos validados
					* Validamos que el response de los datos tenga información
					*/
					if (datosJSON.response.dataResponse && bResp) {
						//Validar DATOS del trabajador
						if (datosJSON.response.dataResponse.respuestaComparacion) {

							if (datosJSON.response.dataResponse.respuestaComparacion.codigoRespuestaDatos != null)
								iRespDatos = datosJSON.response.dataResponse.respuestaComparacion.codigoRespuestaDatos;

							if (datosJSON.response.dataResponse.respuestaComparacion.curp != null)
								bCurp = datosJSON.response.dataResponse.respuestaComparacion.curp;

							if (datosJSON.response.dataResponse.respuestaComparacion.ocr != null) {
								bOcr = datosJSON.response.dataResponse.respuestaComparacion.ocr;
								bCic = datosJSON.response.dataResponse.respuestaComparacion.ocr;
							}

							if (datosJSON.response.dataResponse.respuestaComparacion.nombre != null)
								bNombres = datosJSON.response.dataResponse.respuestaComparacion.nombre;

							if (datosJSON.response.dataResponse.respuestaComparacion.apellidoPaterno != null)
								bAppaterno = datosJSON.response.dataResponse.respuestaComparacion.apellidoPaterno;

							if (datosJSON.response.dataResponse.respuestaComparacion.apellidoMaterno != null)
								bApmaterno = datosJSON.response.dataResponse.respuestaComparacion.apellidoMaterno;

							if (datosJSON.response.dataResponse.respuestaComparacion.anioEmision != null)
								bAnioemision = datosJSON.response.dataResponse.respuestaComparacion.anioEmision;

							if (datosJSON.response.dataResponse.respuestaComparacion.numeroEmisionCredencial != null)
								bNumeroemision = datosJSON.response.dataResponse.respuestaComparacion.numeroEmisionCredencial;

							if (datosJSON.response.dataResponse.respuestaComparacion.claveElector != null)
								bClaveelector = datosJSON.response.dataResponse.respuestaComparacion.claveElector;

							if (datosJSON.response.dataResponse.respuestaComparacion.anioRegistro != null)
								bAnioregistro = datosJSON.response.dataResponse.respuestaComparacion.anioRegistro;
						}

						if (datosJSON.response.dataResponse.respuestaSituacionRegistral) {
							cRespSitucacion = datosJSON.response.dataResponse.respuestaSituacionRegistral.tipoSituacionRegistral;
							cRespRobo = datosJSON.response.dataResponse.respuestaSituacionRegistral.tipoReporteRoboExtravio;
						}
					}

					/*
					* Validar respuesta del WS-INE
					* Validamos que el response tenga minucias validadas
					* Validamos que el response de las minucias tenga información
					*/
					if (datosJSON.response.minutiaeResponse && bResp) {

						if (datosJSON.response.minutiaeResponse.codigoRespuestaMinucia != null)
							iRespMinucias = datosJSON.response.minutiaeResponse.codigoRespuestaMinucia;

						if (datosJSON.response.minutiaeResponse.similitud2 != null)
							cSimilitudindiceizqwsq = parseFloat(datosJSON.response.minutiaeResponse.similitud2);

						if (datosJSON.response.minutiaeResponse.similitud7 != null)
							cSimilitudindicederwsq = parseFloat(datosJSON.response.minutiaeResponse.similitud7);

						if (porcentajemin <= cSimilitudindiceizqwsq || porcentajemin <= cSimilitudindicederwsq)
							bRespMinucias = true;
					}
						// Validacion de la similitud de los indices
					if ((cSimilitudindiceizqwsq == '0.0%' && cSimilitudindiceizqwsq == '0') && (cSimilitudindicederwsq == '0.0%' && cSimilitudindicederwsq == '0')) {
						reenviointerno = 1;
					}
					else {
						limpiarhuellas();
					}

					/*
					* Obtener el folio INE del WS-INE
					* Obtenemos la fecha en la que se genero la petición
					* Validamos los datos, minucias y vigencia
					*/
					if (datosJSON.timestamp)
						cFolioine = datosJSON.timestamp.indice;

					if (datosJSON.response.fechaHoraPeticion)
						tFechatimestamp = datosJSON.response.fechaHoraPeticion;

					if (arrResp.estatus == 5) {
						console.log("cConsultaservine->" + cConsultaservine + " iRespDatos-> " + iRespDatos + " iRespMinucias->" + iRespMinucias + " cRespSitucacion->" + cRespSitucacion + " bRespMinucias->" + bRespMinucias + " cRespRobo->" + cRespRobo);
						if (cConsultaservine == 0) {
							//Validar que la credencial este vigente
							if (cRespSitucacion == 'VIGENTE') {
								//Validar que no este robada o estraviada la credencial
								if (cRespRobo == null) {
									//Validar que las minucias cumplan con la calidad
									if (bRespMinucias) {
										if (bOcr == true && bNombres == true && bAppaterno == true && bAnioregistro == true && bNumeroemision == true && bClaveelector == true && (bApmaterno == true || bApmaterno == null)) {
											cConsultawsineServ = 'SI';
											cConsultawsine = 'SI';
											bRespOpc = 1;
										} else {
											//Fallo la validación de la tabla de true en los datos del trabajador
											cRespServicio = 1004;
											bRespOpc = 2;
										}
									} else {
										//Las minucias no cumplen con la calidad minima requerida
										cRespServicio = 1003;
										bRespOpc = 2;
									}
								}
								/* folio 1255.1 
								se implemento a finales de marzo del 2020
								programador: 98739514
								Por indicacion del usuario Natalia Escobar se agrego Flujo
								cRespSitucacion == 'VIGENTE' => cRespRobo == 'REPORTE_DE_ROBO' || cRespRobo == 'REPORTE_DE_EXTRAVIO' codigo: 1005
								*/
								else if(cRespRobo == 'REPORTE_DE_ROBO' || cRespRobo == 'REPORTE_DE_EXTRAVIO')
								{
										cRespServicio = 1005;
										bRespOpc = 2;
								}
								else 
								{
									//Validar que la credencias no sea robada
									
									/* folio 1255.1 
									se implemento a finales de marzo del 2020
									programador: 98739514
									Por indicacion del usuario Natalia Escobar se cambia cRespServicio de 1002 a 1005
									*/
									//cRespServicio = 1002;
									cRespServicio = 1005;
									bRespOpc = 2;
								}
							} 
							/* folio 1255.1 
							se implemento a finales de marzo del 2020
							programador: 98739514
							Por indicacion del usuario Natalia Escobar se agrego Flujo
							(cRespSitucacion == 'DATOS_NO_ENCONTRADOS' ||  
							cRespSitucacion == 'NO_VIGENTE') codigo: 1005
							*/
							else if (cRespSitucacion == 'DATOS_NO_ENCONTRADOS' ||  
									 cRespSitucacion == 'NO_VIGENTE') 
							{ 
								//Validar que tipoSituacionRegistral es no vigente o datos no encontrados
								//valida que tipoReporteRoboExtravio sea reporte de robo
								cRespServicio = 1005;
								bRespOpc = 2;
							}  
							else 
							{
								//Los datos del promotor no estan NO_VIGENTE o DATOS_NO_ENCONTRADOS
								//Validar que la credencias no sea robada
								
								/* folio 1255.1 
								se implemento a finales de marzo del 2020
								programador: 98739514
								Por indicacion del usuario Natalia Escobar se cambia cRespServicio de 1001 a 1005
								*/
								//cRespServicio = 1001;
								cRespServicio = 1005;
								bRespOpc = 2;
							}
						} else {
							//El codigo no es 0 Exitoso
							cRespServicio = cConsultaservine;
							bRespOpc = 2;
						}
						fnejecutaconsentimiento(folioapi);//////////////////
						if (actualizaBitacoraIne()) {
							if (bRespOpc > 0) {
								console.log("contadorintentos-> " + contadorintentos + " < numerointentos-> " + numerointentos);
								validarbotonesine();
								switch (bRespOpc) {
									case 1:
										//llamar python
										if (banderaActIvr == 0) {
											medioenvio = dEnvio[1];
										}
										else if (banderaActIvr == 1) {
											medioenvio = medioenvio;
										}

										datosinsertarsms = folioapi + ",'" + cCurp + "'," + companiacel + "," + medioenvio + ",'" + telefono + "', 1";
										insertardatossms(datosinsertarsms);
										break;
									case 2:
										if (contadorintentos < numerointentos) {
											if(reenviointerno == 1 && opcEnvio == 1){
												detonarServicioIne(2);
											}else{
												obtenermensajecodigoresp(cRespServicio);
											}

										} else {
											if (OSName == 'Android') {
												var sMensaje = mensajeTiempo;
												var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
												myDlg.modalcierre(550, 250, cTituloModal);
												myDlg.mostrar(sMensaje);
											} else {
												if (iUtileria == 2) {
													var cmensaje = 'Colaborador: Has agotado los intentos de Autenticación, deberás intentarlo más tarde, esto fue debido a que la información enviada fue incorrecta en más de 2 ocasiones.';
												}
												else {
													if(iTipoAfiliacion == 26 || iTipoAfiliacion == 33)
														var cmensaje = 'Promotor, se presentó un inconveniente ante INE, favor de intentar nuevamente';
													else
														var cmensaje = 'Promotor, se presentó un inconveniente ante INE, favor de realizar la afiliación mediante CFEC BanCoppel o Estado de Cuenta de la Afore Actual del Trabajador.';
												}
												mdlMsjCerrar(cTituloModal, cmensaje);
											}
										}
										break;
								}
							} else {
								if (iUtileria == 2) {
									var cmensaje = 'Colaborador, se presentó un inconveniente ante INE, favor de realizar la afiliación mediante CFEC BanCoppel o Estado de Cuenta de la Afore Actual del Solicitante.';
								}
								else {
									if(iTipoAfiliacion == 26 || iTipoAfiliacion == 33)
										var cmensaje = 'Promotor, se presentó un inconveniente ante INE, favor de intentar nuevamente';
									else
										var cmensaje = 'Promotor, se presentó un inconveniente ante INE, favor de realizar la afiliación mediante CFEC BanCoppel o Estado de Cuenta de la Afore Actual del Trabajador.';
								}
								if (OSName == 'Android') {
									var sMensaje = cmensaje;
									var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
									myDlg.modalcierre(500, 250, cTituloModal);
									myDlg.mostrar(sMensaje);
								} else {
									mdlMsjCerrar(cTituloModal, cmensaje);
								}
							}
						}
					} else {
						//no hay registro en bd ws fallo recapturarhuellas
						if (iUtileria == 2) {
							var mensajefnc = 'Colaborador, favor de volver a capturar tus huellas, ocurrio un problema al obtener los datos de la INE.';
							mdlMsjFunc(cTituloModal, mensajefnc, 'aceptar', 'recapturahuellas();');
						}
						else {
							var mensajefnc = 'Promotor, favor de volver a capturar tus huellas, ocurrio un problema al obtener los datos de la INE.';
							mdlMsjFunc(cTituloModal, mensajefnc, 'aceptar', 'recapturahuellas();');
						}
					}

				} else {
					//no hay registro en bd ws fallo recapturarhuellas
					if (iUtileria == 2) {
						var mensajefnc = 'Colaborador, favor de volver a capturar tus huellas, ocurrio un problema con la INE.';
						mdlMsjFunc(cTituloModal, mensajefnc, 'aceptar', 'recapturahuellas();');
					}
					else {
						var mensajefnc = 'Promotor, favor de volver a capturar tus huellas, ocurrio un problema con la INE.';
						mdlMsjFunc(cTituloModal, mensajefnc, 'aceptar', 'recapturahuellas();');
					}
				}
			}else
			{
				fnMensajeSinFolio();
				grabarLog('folioapi' + folioapi);
				$("#boxT").show();
				$('#myModal').modal('hide');
				document.getElementById("valine").disabled = false;
				//	mensajeT -> obtenido de la tabla cat_enlacesine
				$("#texoM").text(mensajeT);
					if(contadori == 3)
					{	
						if(iTipoAfiliacion == 26 || iTipoAfiliacion == 33)
						{
							grabarLog('cantidad de intentos en el boton validar: ' + contadori);
							grabarLog('se presentó un inconveniente ante INE, obtenfion de folioapi sin éxito ' + folioapi);
							var cmensaje = 'Promotor, se presentó un inconveniente ante INE, favor de intentar nuevamente';
							mdlMsjCerrar(cTituloModal, cmensaje);
						}
						else
						{
							grabarLog('cantidad de intentos en el boton validar: ' + contadori);
							grabarLog('se presentó un inconveniente ante INE, obtenfion de folioapi sin éxito ' + folioapi);
							var cmensaje = 'Promotor, se presentó un inconveniente ante INE, favor de realizar la afiliación mediante CFEC BanCoppel o Estado de Cuenta de la Afore Actual del Trabajador.';
							mdlMsjCerrar(cTituloModal, cmensaje);
						}
					}	
				
			}				
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function validarbotonesine() {
	if (bNombres == false) {
		$("#nombres").parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
		$("#nombres").next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
	} else {
		$("#nombres").prop('disabled', true);
	}

	if (bOcr == false) {
		$(ocrine).parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
		$(ocrine).next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
	} else {
		$("#ocrine").prop('disabled', true);
	}

	if (bCic == false) {
		$(cicine).parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
		$(cicine).next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
	} else {
		$("#cicine").prop('disabled', true);
	}

	if (bAppaterno == false) {
		$(apellidop).parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
		$(apellidop).next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
	} else {
		$("#apellidop").prop('disabled', true);
	}

	if (bApmaterno == false) {
		$(apellidom).parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
		$(apellidom).next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
	} else {
		$("#apellidom").prop('disabled', true);
	}

	if (bAnioregistro == false) {
		$(anioreg).parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
		$(anioreg).next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
	} else {
		$("#anioreg").prop('disabled', true);
	}

	if (bNumeroemision == false) {
		$(numemision).parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
		$(numemision).next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
	} else {
		$("#numemision").prop('disabled', true);
	}

	if (bClaveelector == false) {
		$(clvelector).parents(".input-val").removeClass("has-success-jqval").addClass("has-error-jqval");
		$(clvelector).next("span.feedback-jqval").removeClass("fa-check").addClass("fa-times");
	} else {
		$("#clvelector").prop('disabled', true);
	}
}

function insertardatossms(datos) {
	var arrResp = new Array();
	var respuestasms = 0;
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS03", arrdatos: datos },
		success: function (data) {
			arrResp = (data.CONS03);
			respuestasms = arrResp[0].respuesta;
			if (respuestasms == 1) {
				if (medioenvio == dEnvio[0]) {
					fninvocallamadaivr(cCurp);
				}
				else if (medioenvio == dEnvio[1]) {
					llamarpython(folioapi);
				}
			}

			else {
				if (iUtileria == 2) {
					mdlMsj(cTituloModal, 'Colaborador, se tuvo un problema al guardar los datos del teléfono, favor de contactar a mesa de ayuda!');
					$("#valine").prop('disabled', false);
				}
				else {
					mdlMsj(cTituloModal, 'Promotor, se tuvo un problema al guardar los datos del teléfono, favor de contactar a mesa de ayuda!');
					$("#valine").prop('disabled', false);
				}
			}

		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

/**************************************************************************************************************************************
 * <CONSUMIR PYTHON>
**************************************************************************************************************************************/
function llamarpython(folioservicio) {
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 5, folioserv: folioservicio },
		success: function (data) {
			arrResp = data;
			if (OSName == 'Android') {
				Android.volverMenuIne('1');
			} else {
				//Cerrar Modal cargando 
				$('.modal-content').remove();
				cerrarNavegador();// --DESCOMENTAR
			}

		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function mostrarImgIneIfe(val) {
	if (val == 1)//imagenes INE
	{
		document.getElementById('imgnumemi').src = "img/INE_NumEmision.png";
		document.getElementById('imgclaveelec').src = "img/INE_ClaveElector.png";
		document.getElementById('imgaanio').src = "img/INE_AnoRegistro.png";
		document.getElementById('imgcic').src = "img/INE_CIC.png";
		document.getElementById('imgocr').src = "img/INE_OCR.png";
		document.getElementById("imgocr").classList.remove("imgni");
		document.getElementById("imgocr").classList.add("imgn");
	}
	else if (val == 2)//imagenes IFE
	{
		document.getElementById('imgnumemi').src = "img/IFE_NumEmision.png";
		document.getElementById('imgclaveelec').src = "img/IFE_ClaveElector.png";
		document.getElementById('imgaanio').src = "img/IFE_AnoRegistro.png";
		document.getElementById('imgocr').src = "img/IFE_OCR.png";
		document.getElementById("imgocr").classList.remove("imgn");
		document.getElementById("imgocr").classList.add("imgni");
	}
}


function sistemaOperativo() {
	// Instruccion que permite conocer el sistema operativo de donde ingresa el usuario
	if (window.navigator.userAgent.indexOf("Windows NT 10.0") != -1) OSName = "Windows 10";
	if (window.navigator.userAgent.indexOf("Windows NT 6.2") != -1) OSName = "Windows 8";
	if (window.navigator.userAgent.indexOf("Windows NT 6.1") != -1) OSName = "Windows 7";
	if (window.navigator.userAgent.indexOf("Windows NT 6.0") != -1) OSName = "Windows Vista";
	if (window.navigator.userAgent.indexOf("Windows NT 5.1") != -1) OSName = "Windows XP";
	if (window.navigator.userAgent.indexOf("Windows NT 5.0") != -1) OSName = "Windows 2000";
	if (window.navigator.userAgent.indexOf("Mac") != -1) OSName = "Mac/iOS";
	if (window.navigator.userAgent.indexOf("X11") != -1) OSName = "UNIX";
	if (window.navigator.userAgent.indexOf("Linux") != -1) OSName = "Linux";
	if (window.navigator.appVersion.indexOf("Android") != -1) OSName = "Android";
}

// MOVIL
/* Obtener parametros get desde el webview android */
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// MOVIL
/* Funcion que recibe la respuesta del componente EnrolMovil */
function getComponenteHuellaIne(jsonDerecho, jsonIzquierdo) {

	curpyfechader = jsonDerecho;
	curpyfechaizq = jsonIzquierdo;

	iOpcion = SUBIR_ARCHIVO;
	iOpcionHuella = PUBLICA_HUELLAS_DER;

	respuestaWebService(1, iOpcion);

}

/**************************************************************************************************************************************
 * <CONSUMIR LLAMADA IVR>
**************************************************************************************************************************************/
function fninvocallamadaivr(cCurp) {
	var arrResp = new Array();
	var folioAutenticacion = 0;
	var arrDataEnvio = new Array();
	cCurp = "'" + cCurp.toUpperCase() + "'";

	if (iUtileria == 2) {
		arrDataEnvio = { opcion: 3, idcons: "CONS17", arrdatos: cCurp };

	}
	else {
		arrDataEnvio = { opcion: 3, idcons: "CONS14", arrdatos: cCurp };
	}

	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: arrDataEnvio,
		success: function (data) {
			if (iUtileria == 2) {
				arrResp = (data.CONS17);
			}
			else {
				arrResp = (data.CONS14);
			}
			folioAutenticacion = arrResp[0].respuesta;
			if (folioAutenticacion != '') {
				iOpcion = URL_LLAMADA_IVR;
				ligaCaseLlamada = obtenerligas(iOpcion);
				llamadaivr(folioAutenticacion);
			}
			else {
				if (iUtileria == 2) {
					mdlMsj(cTituloModal, 'Colaborador, se tuvo un problema al generar el folio de autenticación INE, favor de contactar a mesa de ayuda!');
					$("#valine").prop('disabled', false);
				}
				else {
					mdlMsj(cTituloModal, 'Promotor, se tuvo un problema al generar el folio de autenticación INE, favor de contactar a mesa de ayuda!');
					$("#valine").prop('disabled', false);
				}

			}
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}


function fnactivaivr() {
	var arrResp = new Array();

	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS15" },
		success: function (data) {
			arrResp = (data.CONS15);
			banderaActIvr = arrResp[0].respuesta;
			console.log('activar llamada ivr: ' + banderaActIvr);
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function llamadaivr(folioAutenticacion) {
	var arrResp;

	esUtileria = iTipoAfiliacion == 26 || iTipoAfiliacion == 33 ? 3 : esUtileria;
	
	var urlIvr = ligaCaseLlamada + "?telefono="+telefono+"&codautent="+folioAutenticacion+"&opcion="+esUtileria;
	
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 6, url: urlIvr },
		success: function (data) {
		
			data = JSON.parse(data);
			if (data.estatus == 1) {
				//cerrarNavegador();
				fnactualizarrespuestallamadaivr(folioapi);
			}
			else {
				if (esUtileria == 2) {
					mdlMsj(cTituloModal, 'Colaborador, se tuvo un problema al generar la llamada IVR, favor de contactar a mesa de ayuda!');
					$("#valine").prop('disabled', false);
				}
				else {
					mdlMsj(cTituloModal, 'Promotor, se tuvo un problema al generar la llamada IVR, favor de contactar a mesa de ayuda!');
					$("#valine").prop('disabled', false);
				}
			}
			//cerrarNavegador();

		}, error: function (a, b, c) {
			var sMensaje = "error ajax " + Object.values(a) + " " + b + " " + c ;
			var myDlg = new dialog_ac(document.getElementById('divDlgMain'));
			myDlg.modalCerrar(500, 250, cTituloModal);
			myDlg.mostrar(sMensaje);
			//alert(sMensaje);
			
		}
	});
}

function fnejecutaconsentimiento(folioapi) {
	if (folioapi != '') {
		iOpcion = URL_CONSENTIMIENTO;
		ligaCaseLlamada = obtenerligas(iOpcion);
	}
	var arrResp = new Array();
	$.ajax({
		async: false,
		cache: false,
		url: '../'+ligaCaseLlamada,
		type: 'GET',
		dataType: 'JSON',
		data: { foliosolicitud: folioapi },
		success: function (data) {
			//console.log('SE EJECUTO fnejecutaconsentimiento' + data);
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}



function fnactualizarrespuestallamadaivr(folioapi) {
	var arrResp = new Array();
	var actualiza = 0;
	var valores = folioapi + ",1";
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS16", arrdatos: valores },
		success: function (data) {
			arrResp = (data.CONS16);
			actualiza = arrResp[0].respuesta;
			if (actualiza == 1) {

				cerrarNavegador();
			}

		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function limpiarhuellas() {
	var arrDatos = {
		"minucia2": curpyfechader,
		"minucia7": curpyfechaizq
	};

	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 4, idcons: "limpiarhuellas", arrdatos: arrDatos },
		success: function (data) {
			arrResp = (data);
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	});
}

function validarCampos(params) {
	$("#celular").focusout(function () {
		hablitarValidarIne();
	});

	$("#Confcelular").focusout(function () {
		hablitarValidarIne();
	});

	$("#nombres").focusout(function () {
		hablitarValidarIne();
	});

	$("#apellidop").focusout(function () {
		hablitarValidarIne();
	});

	$("#apellidom").focusout(function () {
		hablitarValidarIne();
	});

	$("#numemision").focusout(function () {
		hablitarValidarIne();
	});

	$("#clvelector").focusout(function () {
		hablitarValidarIne();
	});

	$("#anioreg").focusout(function () {
		hablitarValidarIne();
	});

	$("#cicine").focusout(function () {
		hablitarValidarIne();
	});

	$("#ocrine").focusout(function () {
		hablitarValidarIne();
	});

	$("#compania").focusout(function () {
		hablitarValidarIne();
	});

	$("#medioenvio").focusout(function () {
		hablitarValidarIne();
	});
}

function hablitarValidarIne() {
	$("#valine").prop('disabled', true);

	$("#boxT").hide();
	if ($("#celular").val() == "") {
		return;
	}

	if($("#Confcelular").val() == ""){
		return;
	}

	if ($("#nombres").val() == "") {
		return;
	}

	/*if ($("#apellidop").val() == "" ) {
		return;
	}

	if ($("#apellidom").val() == "") {
		return;
	}*/

	if ($("#numemision").val() == "") {
		return;
	}


	if ($("#clvelector").val() == "") {
		return;
	}


	if ($("#anioreg").val() == "") {
		return;
	}


	if ($("#cicine").val() == "" && $("#ird1").is(":checked") == true) {
		return;
	}

	if ($("#ocrine").val() == "") {
		return;
	}

	if ($("#compania").val() == "") {
		return;
	}

	if ($("#medioenvio").val() == "") {
		return;
	}

	$("#valine").prop('disabled', false);
}

function validarCaracteresEspeciales(sTexto) {
	var arrTexto = sTexto.split(" ");
	var sTextoFinal = "";

	if (arrTexto.length > 0) {
		for (i = 0; i < arrTexto.length; i++) {
			arrTexto[i] = arrTexto[i].replace("'", "''");
		}

		for (i = 0; i < arrTexto.length; i++) {
			sTextoFinal += arrTexto[i] + " ";
		}
	}

	return sTextoFinal.trim();

}

function utf8_decode(utftext)
{
	var string = "";
	var i = 0;
	var c = c1 = c2 = 0;
	while ( i < utftext.length ) {
		c = utftext.charCodeAt(i);
		if (c < 128) {
			string += String.fromCharCode(c);
			i++;
		}
		else if((c > 191) && (c < 224)) {
			c2 = utftext.charCodeAt(i+1);
			string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
			i += 2;
		}
		else {
			c2 = utftext.charCodeAt(i+1);
			c3 = utftext.charCodeAt(i+2);
			string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
			i += 3;
		}
	}
	return string;
}


function detonarServicioIne(opc) {
	/** 
	 * opc = 1-Envio normal / 2-Renvio interno
	*/
	if(opc==2){reenviointerno = 0;}
	else{reenviointerno = 0;}
	
	if (guardarDatosIne()) {
		if (obtenerUbicacion()) {
			if (ejecutarApiINE()) {
				setTimeout(function () {
					//SETTIME DE 5 SEG POR EL INE
					consultardatosine(opc);
				}, iMilisegundos);
			}
		}
	}
}

//funcion Pare generar log
function grabarLog(cTexto)
{
	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 1, mensaje: cTexto},
		success: function (data) {
			//log guardado
			console.log("log guardado con exito");
		}, error: function (a, b, c) {
			console.log(" problema al guardar el log");
		}
	});
}

function fnMensajeSinFolio() {
	//grabarLog("Dentro de la funcion fnMensajeSinFolio" );
	var arrResp = new Array();

	$.ajax({
		async: false,
		cache: false,
		url: ligaCase,
		type: 'POST',
		dataType: 'JSON',
		data: { opcion: 3, idcons: "CONS19" },
		success: function (data) {
			arrResp = (data.CONS19);
			mensajeT = arrResp[0].mensaje;
			console.log('activar llamada ivr: ' + mensajeT);
			grabarLog("Se obtiene el Mensaje: " + mensajeT);
		}, error: function (a, b, c) {
			alert("error ajax " + a + " " + b + " " + c + " favor de volver a intentar el tramite, si el problema persiste favor de contactar a mesa de ayuda.");
		}
	}); 
}