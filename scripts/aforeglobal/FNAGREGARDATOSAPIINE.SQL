-- Function: fnagregardatosapiine(character, character, character, character, character, character, character, character, integer, character, text, text, character,integer)

-- DROP FUNCTION fnagregardatosapiine(character, character, character, character, character, character, character, character, integer, character, text, text, character,integer);

CREATE OR REPLACE FUNCTION fnagregardatosapiine(character, character, character, character, character, character, character, character, integer, character, text, text, character,integer)
  RETURNS integer AS
$BODY$

DECLARE        
    cCurp ALIAS FOR $1;
    iOcr ALIAS FOR $2;
    iCic ALIAS FOR $3;
    cNombres ALIAS FOR $4;
    cAppaterno ALIAS FOR $5;
    cApmaterno ALIAS FOR $6;
    iAnioregistro ALIAS FOR $7;
    iNumeroemisioncredencial ALIAS FOR $8;
    iEmpleado ALIAS FOR $9;
    clvelector ALIAS FOR $10;
    tIndiceizqwsq ALIAS FOR $11;
    tIndicederwsq ALIAS FOR $12;
    cIpmodulo     ALIAS FOR $13;
    iUtileria     ALIAS FOR $14;
     iRespuesta INTEGER;

BEGIN	

    INSERT INTO ineautenticacionhuellas (curp,ocr, cic, nombres, appaterno, apmaterno, anioregistro,  numeroemisioncredencial, empleado, idestadoine, claveelector, indiceizqwsq, indicederwsq, ipmodulo, utileria ) VALUES (cCurp,iOcr, iCic, cNombres, cAppaterno, cApmaterno, iAnioregistro, iNumeroemisioncredencial, iEmpleado, 1, clvelector, tIndiceizqwsq, tIndicederwsq,cIpmodulo, iUtileria);

          IF FOUND THEN
                iRespuesta  = 1;

          ELSE 
                iRespuesta = 0;

          END IF;

              Return iRespuesta;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fnagregardatosapiine(character, character, character, character, character, character, character, character, integer, character, text, text, character,integer)
  OWNER TO postgres;
GRANT EXECUTE ON FUNCTION fnagregardatosapiine(character, character, character, character, character, character, character, character, integer, character, text, text, character,integer) TO public;
GRANT EXECUTE ON FUNCTION fnagregardatosapiine(character, character, character, character, character, character, character, character, integer, character, text, text, character,integer) TO postgres;
GRANT EXECUTE ON FUNCTION fnagregardatosapiine(character, character, character, character, character, character, character, character, integer, character, text, text, character,integer) TO sysaforeglobal;
