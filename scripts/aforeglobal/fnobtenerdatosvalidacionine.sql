-- Function: fnobtenerdatosvalidacionine(integer, integer)

-- DROP FUNCTION fnobtenerdatosvalidacionine(integer, integer);

CREATE OR REPLACE FUNCTION fnobtenerdatosvalidacionine(
    integer,
    integer)
  RETURNS SETOF tpobtenerdatosvalidacionine AS
$BODY$

DECLARE 
    iFoliosoline    ALIAS FOR $1;
    iRespuesta  ALIAS FOR $2;
    iUtileria  integer;
    cMensaje    CHAR(100);
    cSemilla    TEXT;
	cFolioautenticacion TEXT;
    cCurp       CHAR(18);
    regresa tpobtenerdatosvalidacionine;
    
    
BEGIN
    regresa.llavepublica    = '';
    regresa.llaveprivada    = '';
    regresa.mensaje     = '';
    regresa.telefono    = '';
    regresa.url         = '';
    regresa.respuesta   = 0;
    IF EXISTS (SELECT 'X' FROM tbenviosmsvalidacionine WHERE foliosolicitudine  = iFoliosoline) THEN
        IF iRespuesta = -5 THEN
            SELECT curp,telefono INTO cCurp ,regresa.telefono FROM tbenviosmsvalidacionine WHERE foliosolicitudine  = iFoliosoline; 
	    SELECT utileria INTO iUtileria FROM ineautenticacionhuellas where foliosoline = iFoliosoline;
            --OBTENER LA LLAVE PUBLICA Y PRIVADA PARA EL ENVIO DEL SMS
            SELECT concepto INTO  regresa.llaveprivada FROM cat_enlacesine WHERE id  = 6;
            SELECT concepto INTO  regresa.llavepublica FROM cat_enlacesine WHERE id  = 5;
            SELECT concepto INTO  regresa.url FROM cat_enlacesine WHERE id  = 7;
        --OBTENER EL MENSAJE
	    IF iUtileria = 0 OR iUtileria = 1 THEN -- TRASPASOS 0, MOVIL 1
		--OBTENER LA SEMILLA DE LA CURP QUE SE REMPLAZARA POR "<<CODIGO>>"
            SELECT TRIM(fngeneracioncodigoautenticacion) AS semilla INTO cSemilla FROM fngeneracioncodigoautenticacion(cCurp);
			SELECT TRIM(concepto) AS mensaje INTO cMensaje FROM pafhardcode WHERE tipo1 = 228 AND tipo2 = 1 AND tipo3 = 1;
			--CREAR MENSAJE
            regresa.mensaje =REPLACE(cMensaje, '<<CODIGO>>', cSemilla) ;
            regresa.respuesta = 1;    
		ELSIF iUtileria = 2 THEN -- SERVICIOS
			SELECT TRIM(fngenerafolioautenticacionservicios) AS folioautenticacion INTO cFolioautenticacion FROM fngenerafolioautenticacionservicios(cCurp);
			SELECT TRIM(concepto) AS mensaje INTO cMensaje FROM pafhardcode WHERE tipo1 = 232 AND tipo2 = 1 AND tipo3 = 1;
			--CREAR MENSAJE
            regresa.mensaje =REPLACE(cMensaje, '<<CODIGO>>', cFolioautenticacion) ;
            regresa.respuesta = 1;
		END IF;
                   
        ELSE
            UPDATE tbenviosmsvalidacionine SET respuesta = iRespuesta, intentos = (intentos+1) WHERE foliosolicitudine = iFoliosoline;
            IF FOUND THEN
                regresa.mensaje = 'EXITO';
                regresa.respuesta = 2;
            END IF;
        END IF;
    END IF;
    RETURN NEXT regresa;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION fnobtenerdatosvalidacionine(integer, integer)
  OWNER TO sysaforeglobal;
GRANT EXECUTE ON FUNCTION fnobtenerdatosvalidacionine(integer, integer) TO sysaforeglobal;
GRANT EXECUTE ON FUNCTION fnobtenerdatosvalidacionine(integer, integer) TO public;
GRANT EXECUTE ON FUNCTION fnobtenerdatosvalidacionine(integer, integer) TO postgres;
