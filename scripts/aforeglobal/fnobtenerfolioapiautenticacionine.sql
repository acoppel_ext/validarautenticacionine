-- Function: fnobtenerfolioapiautenticacionine(character)

-- DROP FUNCTION fnobtenerfolioapiautenticacionine(character);

CREATE OR REPLACE FUNCTION fnobtenerfolioapiautenticacionine(character)
  RETURNS text AS
$BODY$


DECLARE 
	cCurp 			ALIAS FOR $1;

	cFoliosoline TEXT;
	
BEGIN
	
	--Obtener el foliosolicitudine
	SELECT foliosolicitudine INTO cFoliosoline FROM tbenviosmsvalidacionine WHERE curp = cCurp AND fechaalta::DATE = CURRENT_DATE ORDER BY fechaalta DESC LIMIT 1; 

	-- Devolver el folio de autenticación generado
	RETURN cFoliosoline;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fnobtenerfolioapiautenticacionine(character)
  OWNER TO sysaforeglobal;
