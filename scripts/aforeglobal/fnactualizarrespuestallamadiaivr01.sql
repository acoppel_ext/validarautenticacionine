-- Function: fnactualizarrespuestallamadiaivr01(integer)

-- DROP FUNCTION fnactualizarrespuestallamadiaivr01(integer, integer);

CREATE OR REPLACE FUNCTION fnactualizarrespuestallamadiaivr01(integer, integer)
  RETURNS integer AS
$BODY$
DECLARE 
	iFolioIne 	ALIAS FOR $1;
	iAutentica 	ALIAS FOR $2;
	iActualizo 	INTEGER;
BEGIN
	iActualizo = 0;
	
	UPDATE tbenviosmsvalidacionine SET respuesta = 1, intentos = (intentos + 1) where foliosolicitudine = iFolioIne AND autenticacion = iAutentica ;
	IF FOUND THEN
		iActualizo = 1;
	END IF;

	RETURN iActualizo;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE SECURITY DEFINER
  COST 100;
ALTER FUNCTION fnactualizarrespuestallamadiaivr01(integer, integer)
  OWNER TO sysaforeglobal;
GRANT EXECUTE ON FUNCTION fnactualizarrespuestallamadiaivr01(integer, integer) TO public;
GRANT EXECUTE ON FUNCTION fnactualizarrespuestallamadiaivr01(integer, integer) TO sysaforeglobal;
GRANT EXECUTE ON FUNCTION fnactualizarrespuestallamadiaivr01(integer, integer) TO postgres;