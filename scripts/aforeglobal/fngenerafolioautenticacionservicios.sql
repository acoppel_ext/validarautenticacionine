-- Function: fngenerafolioautenticacionservicios(character)

-- DROP FUNCTION fngenerafolioautenticacionservicios(character);

CREATE OR REPLACE FUNCTION fngenerafolioautenticacionservicios(character)
  RETURNS character AS
$BODY$


DECLARE 
	cCurp 			ALIAS FOR $1;
	dFecha TEXT;
	iConsecutivo INTEGER;
	cConsecutivo CHARACTER (6);
	cfolioautenticacion CHARACTER(14);
	iFoliosoline INTEGER;
	
BEGIN
	-- Inicializar variables
 	cfolioautenticacion = '';
	dFecha = '';
	iConsecutivo = 0;
	cConsecutivo = '';
	
	IF EXISTS (SELECT folioautenticacion FROM controlfolioautenticacionineservicios WHERE curp = cCurp AND (folioconfirma != '' OR folioconfirma != NULL) AND fechaalta::DATE = CURRENT_DATE ORDER BY fechaalta DESC LIMIT 1) THEN
			--Obtener la fecha con formato YYMMDDHH
		SELECT TO_CHAR(NOW(), 'YYmmddHH24') INTO dFecha;
		--Validar el consecutivo si la tabla esta vacia entonces el iConsecutivo sería 1 para el primer registro
		IF (SELECT COUNT(*) FROM controlfolioautenticacionineservicios) = 0 THEN
			iConsecutivo = 1;
		ELSE -- Sacar el ultimo consecutivo y sumarle 1 al maximo keyx registrado
			SELECT (consecutivo::INTEGER)+1 INTO iConsecutivo FROM controlfolioautenticacionineservicios WHERE keyx = (SELECT MAX(keyx) FROM controlfolioautenticacionineservicios);
		END IF;
		
		IF iConsecutivo > 999999 THEN -- Validarlo para reiniciarlo a 000001
		   iConsecutivo = 1;
		END IF;
		
		--Insertar 0 a la izquierda del consecutivo
		SELECT LPAD(iConsecutivo,6,'0') INTO cConsecutivo;
		--Concatenar las fechas y el consecutivo para crear el folioautenticacion
		cfolioautenticacion = dFecha||cConsecutivo;
		
		--INSERTAR EL FOLIO AUTENTICACION 
		SELECT foliosolicitudine INTO iFoliosoline FROM tbenviosmsvalidacionine WHERE curp  = cCurp AND fechaalta::DATE = CURRENT_DATE ORDER BY fechaalta DESC LIMIT 1;
		
		INSERT INTO controlfolioautenticacionineservicios (folioautenticacion,ifoliosoline,curp,consecutivo) VALUES (cfolioautenticacion,iFoliosoline,cCurp,cConsecutivo);
		
	ELSIF EXISTS(SELECT folioautenticacion FROM controlfolioautenticacionineservicios WHERE curp = cCurp AND fechaalta::DATE = CURRENT_DATE ORDER BY fechaalta DESC LIMIT 1) THEN
		SELECT folioautenticacion INTO cfolioautenticacion FROM controlfolioautenticacionineservicios WHERE curp  = cCurp AND fechaalta::DATE = CURRENT_DATE ORDER BY fechaalta DESC LIMIT 1;	
	ELSE
		--Obtener la fecha con formato YYMMDDHH
		SELECT TO_CHAR(NOW(), 'YYmmddHH24') INTO dFecha;
		--Validar el consecutivo si la tabla esta vacia entonces el iConsecutivo sería 1 para el primer registro
		IF (SELECT COUNT(*) FROM controlfolioautenticacionineservicios) = 0 THEN
			iConsecutivo = 1;
		ELSE -- Sacar el ultimo consecutivo y sumarle 1 al maximo keyx registrado
			SELECT (consecutivo::INTEGER)+1 INTO iConsecutivo FROM controlfolioautenticacionineservicios WHERE keyx = (SELECT MAX(keyx) FROM controlfolioautenticacionineservicios);
		END IF;
		
		IF iConsecutivo > 999999 THEN -- Validarlo para reiniciarlo a 000001
		   iConsecutivo = 1;
		END IF;
		
		--Insertar 0 a la izquierda del consecutivo
		SELECT LPAD(iConsecutivo,6,'0') INTO cConsecutivo;
		--Concatenar las fechas y el consecutivo para crear el folioautenticacion
		cfolioautenticacion = dFecha||cConsecutivo;
		
		--INSERTAR EL FOLIO AUTENTICACION 
		SELECT foliosolicitudine INTO iFoliosoline FROM tbenviosmsvalidacionine WHERE curp  = cCurp AND fechaalta::DATE = CURRENT_DATE ORDER BY fechaalta DESC LIMIT 1;
		
		INSERT INTO controlfolioautenticacionineservicios (folioautenticacion,ifoliosoline,curp,consecutivo) VALUES (cfolioautenticacion,iFoliosoline,cCurp,cConsecutivo);				
	END IF;
	-- Devolver el folio de autenticación generado
	RETURN cfolioautenticacion;
END;

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fngenerafolioautenticacionservicios(character)
  OWNER TO sysaforeglobal;
GRANT EXECUTE ON FUNCTION fngenerafolioautenticacionservicios(character) TO sysaforeglobal;
GRANT EXECUTE ON FUNCTION fngenerafolioautenticacionservicios(character) TO public;
GRANT EXECUTE ON FUNCTION fngenerafolioautenticacionservicios(character) TO postgres;
