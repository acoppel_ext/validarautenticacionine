-- Table: controlfolioautenticacionineservicios

-- DROP TABLE controlfolioautenticacionineservicios;

CREATE TABLE controlfolioautenticacionineservicios
(
  keyx serial NOT NULL,
  fechaalta timestamp without time zone DEFAULT now(),
  folioautenticacion CHARACTER(14) DEFAULT ''::bpchar,
  folioconfirma CHARACTER(14) DEFAULT ''::bpchar,
  ifoliosoline INTEGER,
  curp CHARACTER(18) DEFAULT ''::bpchar,
  consecutivo CHARACTER(6) DEFAULT '000001'
)
WITH (
  OIDS=FALSE
);
ALTER TABLE controlfolioautenticacionineservicios
  OWNER TO sysaforeglobal;
