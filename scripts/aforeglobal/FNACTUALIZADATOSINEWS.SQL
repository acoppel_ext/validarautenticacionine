-- Function: fnactualizadatosinews(integer, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, character, character, character, character, timestamp without time zone, character)

-- DROP FUNCTION fnactualizadatosinews(integer, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, character, character, character, character, timestamp without time zone, character);

CREATE OR REPLACE FUNCTION fnactualizadatosinews(integer, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, character, character, character, character, timestamp without time zone, character)
  RETURNS integer AS
$BODY$

DECLARE   
	iFoliosolicitud ALIAS FOR $1; 
	bValcurp ALIAS FOR $2;
	bValocr ALIAS FOR $3;
	bValcic ALIAS FOR $4;
	bValnombres ALIAS FOR $5;
	bValappaterno ALIAS FOR $6;
	bValapmaterno ALIAS FOR $7;
	bValanioregistro ALIAS FOR $8;
	bValanioemision ALIAS FOR $9;
	bValnumeroemisioncredencial ALIAS FOR $10;
	bValclaveelector ALIAS FOR $11;
	cConsultawsine ALIAS FOR $12;
	cConsultaservicioine ALIAS FOR $13;
	cSimilitudindiceizqwsq ALIAS FOR $14;
	cSimilitudindicederwsq ALIAS FOR $15;
	dFchhrsinesalida    ALIAS FOR $16;
	cFolioine ALIAS FOR $17;

	iRespuesta INTEGER;

	ival       INTEGER:=0;

BEGIN	
	IF  cConsultawsine = 'SI' AND cConsultaservicioine = 'SI' THEN
		ival = 5;
	ELSE 
		ival = 4;
	END IF;

	UPDATE ineautenticacionhuellas 
	SET valcurp = bValcurp, valocr = bValocr, valcic = bValcic, valnombres = bValnombres, valappaterno = bValappaterno, 
	valapmaterno = bValapmaterno, valanioregistro = bValanioregistro, valanioemision = bValanioemision, valnumeroemisioncredencial = bValnumeroemisioncredencial,
	valclaveelector = bValclaveelector, 
	consultawsine = cConsultawsine, consultaservicioine = cConsultaservicioine, 
	similitudindiceizqwsq =  cSimilitudindiceizqwsq, similitudindicederwsq = cSimilitudindicederwsq,
	fchhrsinesalida = dFchhrsinesalida, folioine = cFolioine, idestadoine = ival 
	WHERE foliosoline = iFoliosolicitud;

	IF FOUND THEN
		iRespuesta  = 1;
	ELSE 
		iRespuesta = 0;
	END IF;

	RETURN iRespuesta;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fnactualizadatosinews(integer, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, character, character, character, character, timestamp without time zone, character) OWNER TO postgres;
GRANT EXECUTE ON FUNCTION fnactualizadatosinews(integer, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean, character, character, character, character, timestamp without time zone, character) TO sysaforeglobal;