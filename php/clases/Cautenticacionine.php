<?php
include_once("../../distboostrap4/php/definiciones.php");
include_once('Capirestautenticacionine.php');

class CGeneral extends CMetodoGral
{
	public static $cNombreLog = "validarautenticacionine";

	//OPCION 1
	public static function grabarRegistro($texto)
	{
		//dar el nombre del log
		CMetodoGral::setLogName(CGeneral::$cNombreLog);
		CMetodoGral::grabarLogx("[" . __METHOD__ . "]" . $texto);
		$arrDatos = array("texto" => $texto);
		return $arrDatos;
	}

	//OPCION 2
	public static function datosPagina(){
		$arrDatos 	= array(
			"irespuesta" 	=> CONSOK__,
			"fechaActual"	=> "",
			"ipspa"			=> "",
			"ipmodulo"		=> ""
		);

		//Obtenemos la fecha actual y se la mandamos como parametro
		setlocale(LC_TIME, "es_ES");
		$cFechaDia 					= utf8_encode(strftime("%A, %d de %B de %Y"));
		$arrDatos["fechaActual"] 	= $cFechaDia;
		$arrBD 						= CMetodoGral::getDatosBD("AFOREGLOBAL");
		$arrDatos["ipspa"]			= $arrBD["AFOREGLOBAL"]["BDIP"];
		$arrDatos["ipmodulo"]		= CMetodoGral::getRealIP();

		return $arrDatos;
	}

	public static function consultasProyectos($idCons, $arrOpciones)
	{
		$arrResp = array("irespuesta" => DEFAULT__, $idCons => null);
		$arrCons 	= array(
			"CONS01" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT icodigo,TRIM(cmensaje) AS cmensaje, TRIM(cnombre) AS cnombre, TRIM(ctienda) AS ctienda,espromotor,curppromotor,claveconsar FROM fnConsultarEmpleadoConstancia(<<DATOS>>);"
			),
			"CONS02" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT fnagregardatosapiine AS respuesta  FROM fnagregardatosapiine(<<DATOS>>);"
			),
			"CONS03" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT fnagregardatossmsine01 as respuesta FROM fnagregardatossmsine01(<<DATOS>>);"
			),
			"CONS04" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT clavec, TRIM(desccomp) AS nombre FROM fn_catcompacelulares();"
			),

			"CONS05" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT iestatus, cdescripcion FROM fnmedioenvioine();"
			),
				"CONS06" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT TRIM(nombres), TRIM(apaterno), TRIM(amaterno) FROM fnobtenerdatoscurprenapo(<<DATOS>>);"
			),

			"CONS07" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT repuesta, numerointentos, TRIM(mensaje) AS mensaje, TRIM(concepto) AS porcentaje, milisegundos FROM fnconfiguracionservicioine(<<DATOS>>);"
			),

			"CONS08" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT codigorespuesta, datosrespuesta FROM fnobtenerdatoswsine(<<DATOS>>);"
			),

			"CONS09" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT liga FROM fnobtenerligasservicioine(<<DATOS>>);"
			),
			"CONS10" => array(
				"BDNM" => "BUSTRAMITES",
				"CONS" => "SELECT TRIM(protocolo || ipservidor || ':' || puerto || urlservicio) AS urlwsafore FROM fnobtenerinformacionurlservicio(9943, 0::smallint);"
			),
			"CONS11" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT TRIM(ipofflinelnx) AS ipofflinelnx FROM fnobteneripcluster(<<DATOS>>);"
			),
			"CONS12" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT codigo, TRIM(accion) AS accion, TRIM(mensaje) AS mensaje FROM fnobtenerdiagnosticowsine(<<DATOS>>);"
			),
			"CONS13" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT itienda FROM fnobtenerdatostiendarec(<<DATOS>>);"
			),
			"CONS14" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT fngeneracioncodigoautenticacion AS respuesta FROM fngeneracioncodigoautenticacion(<<DATOS>>);"
			),
			"CONS15" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT fnenviarporivr as respuesta FROM fnenviarporivr(<<DATOS>>);"
			),
			"CONS16" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT fnactualizarrespuestallamadiaivr01 as respuesta from fnactualizarrespuestallamadiaivr01(<<DATOS>>);"
			),
			"CONS17" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT fngenerafolioautenticacionservicios as respuesta from fngenerafolioautenticacionservicios(<<DATOS>>);"
			),
			"CONS18" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT codigo, TRIM(accion) AS accion, TRIM(mensaje) AS mensaje FROM fnobtenermensajeregistroterceroswsine(<<DATOS>>);"
			),
			"CONS19" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT fnmensajeine AS mensaje FROM fnmensajeine();"
			),
			"CONSFECH" => array(
				"BDNM" => "AFOREGLOBAL",
				"CONS" => "SELECT REPLACE(CURRENT_DATE::CHAR(10), '-', '')::CHAR(8) AS fechahuellas;"
			),
		);
			//dar el nombre del log
		CMetodoGral::setLogName(CGeneral::$cNombreLog);
		//Validar que la consulta seleccionada exista
		if (array_key_exists($idCons, $arrCons)) {
			$arrResp["irespuesta"] 	= CONSFOUNDOK__;
			$arrCons 				= $arrCons[$idCons];
			$arrCons["CONS"]		= str_replace("<<DATOS>>", $arrOpciones, $arrCons["CONS"]);
			CMetodoGral::grabarLogx("[" . __METHOD__ . "] CONS->" . $idCons . " BDNM-> " . $arrCons["BDNM"] . " CONS-> " . $arrCons["CONS"]);

			//Ejecutar la consulta obtenida
			$arrConsulta = CMetodoGral::ejecucionBD($idCons, $arrCons);
			if($arrConsulta["irespuesta"] == CONSOK__){
				$arrResp["irespuesta"] 	= $arrConsulta["irespuesta"];
				$arrResp[$idCons] 		= $arrConsulta[$idCons];
			}

		} else {
			$arrResp["irespuesta"] = CONSFOUNDERR__;
			CMetodoGral::grabarLogx("[" . __METHOD__ . "] No encuentra la consulta: " . $idCons);
		}

		return $arrResp;
	}

	public static function llamarapi($consulta,$arrDatosCaptura){

		CMetodoGral::setLogName(CGeneral::$cNombreLog);
		$arrDatos 	= array("estatus" => DEFAULT__, "descripcion" => "");
		$objAPI = new Capirestautenticacionine();
		CMetodoGral::grabarLogx("[" . __METHOD__ . "] Detonar el metodo-> " . $consulta);

		try
		{
			//Ejecuta la consulta
			$resultApi = $objAPI->consumirApi($consulta, $arrDatosCaptura);

			//Verifica que se haya ejecutado correctamente
			if($resultApi)
			{
				$resultApi = json_decode($resultApi,true);

				$arrDatos['estatus'] = APIOK__;
				$arrDatos['respuesta'] = $resultApi;
				CMetodoGral::grabarLogx("[" . __METHOD__ . "] Se obtuvo respuesta de API en la consulta -> ". $consulta);
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje
				$arrDatos['estatus'] = APIERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
				throw new Exception("constanciaafiliacion.php\tConsultacatalogoComprobante"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			CMetodoGral::grabarLogx("[" . __METHOD__ . "] " . $mensaje);
		}

		return $arrDatos;
	}

	public static function leerIP()
	{
		if (!empty($_SERVER["HTTP_CLIENT_IP"]))
			return $_SERVER["HTTP_CLIENT_IP"];

		if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"]))
			return $_SERVER["HTTP_X_FORWARDED_FOR"];

		return $_SERVER["REMOTE_ADDR"];
	}

	public static function python($folioserv)
	{
		CMetodoGral::setLogName(CGeneral::$cNombreLog);
		$output = array();
		CMetodoGral::grabarLogx("*************Inicia proceso python folio-> $folioserv ***************");
		exec("/bin/python /sysx/progs/afore/enviosmsine/enviosmsine.py " . $folioserv . " 1", $output);
		CMetodoGral::grabarLogx("*************Termina proceso python folio-> $folioserv ***************");
		return $output;
	}
	public static function llamarivr($url)
	{
		CMetodoGral::setLogName(CGeneral::$cNombreLog);
		$output = array();
		CMetodoGral::grabarLogx("*************Inicia proceso llamarivr***************");
		
		$res = file_get_contents($url);
		
		CMetodoGral::grabarLogx("URL IVR" . $url);
		CMetodoGral::grabarLogx("Respuesta" . $res);
		
		CMetodoGral::grabarLogx("*************Termina proceso llamarivr***************");
		return $res;
	}
}

?>