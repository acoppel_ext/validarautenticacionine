<?php
	include_once('clases/Cautenticacionine.php');

	$arrResp 	= array();
	//Variables Generales
	$opcion 		= isset($_POST['opcion']) 		? $_POST['opcion'] 			: 0;
	$folioserv 		= isset($_POST['folioserv']) 	? $_POST['folioserv'] 		: 0;
	$empleado 		= isset($_POST['empleado']) 	? $_POST['empleado'] 		: 0;
	$sTexto 		= isset($_POST['mensaje']) 		? $_POST['mensaje'] 		: '';
	$idCons 		= isset($_POST['idcons']) 		? $_POST['idcons'] 			: '';
	$arrOpciones 	= isset($_POST['arrdatos']) 	? $_POST['arrdatos'] 		: '';
	$url			= isset($_POST['url']) 	? $_POST['url'] 		: '';

	if ($idCons == 'CONS11')
	{
		$Ipmod = CGeneral::leerIP();
		$arrOpciones = "'".$Ipmod."'";
	}

	switch ($opcion) {
		case 1: $arrResp = CGeneral::grabarRegistro($sTexto); break;
		case 2: $arrResp = CGeneral::datosPagina(); break;
		case 3: $arrResp = CGeneral::consultasProyectos($idCons, $arrOpciones); break;
		case 4: $arrResp = CGeneral::llamarapi($idCons, $arrOpciones); break;
		case 5: $arrResp = CGeneral::python($folioserv); break;
		case 6: $arrResp = CGeneral::llamarivr($url); break;
		//case 6: $arrResp = CGeneral::leerIP(); break;

	}

	echo json_encode($arrResp);
?>